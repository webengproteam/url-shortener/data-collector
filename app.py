from flask import Flask
from flask_graphql import GraphQLView
from flask_cors import CORS

from data_collector.models import db_session
from data_collector.schema import schema
from data_collector.middleware import type_middleware

app = Flask(__name__)
app.debug = True

view_func = type_middleware(
    GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=True,  # for having the GraphiQL interface
    )
)

app.add_url_rule(
    '/graphql',
    view_func=view_func
)

CORS(app, supports_credentials=True)


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if __name__ == '__main__':
    app.run(host='0.0.0.0')
