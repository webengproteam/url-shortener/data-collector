from tempfile import TemporaryFile

from flask import request, Response


def type_middleware(func):
    def custom_func(*args, **kwargs):
        if not request.accept_mimetypes.accept_json:
            if 'text/csv' in request.accept_mimetypes or \
                    'application/pdf' in request.accept_mimetypes:
                result = func(*args, **kwargs).json

                table = []
                for query in result['data'].keys():
                    first_item = True
                    table += [[query]]
                    for item in result['data'][query]:
                        params = item.keys()
                        if first_item:
                            table += [[param for param in params]]
                            first_item = False
                        line = [item[param] for param in params]
                        table += [line]

                if 'text/csv' in request.accept_mimetypes:
                    import csv

                    temp_file = TemporaryFile(mode='r+', newline='')
                    writer = csv.writer(
                        temp_file,
                        delimiter=',',
                        quotechar='|',
                        quoting=csv.QUOTE_MINIMAL
                    )

                    for row in table:
                        writer.writerow(row)
                    temp_file.seek(0)

                    return Response(temp_file, mimetype='text/csv')

                else:
                    from reportlab.lib.pagesizes import letter, inch
                    from reportlab.platypus import SimpleDocTemplate, Table

                    temp_file = TemporaryFile(mode='r+b')

                    rows = table.__len__()
                    columns = max([row.__len__() for row in table])
                    t = Table(
                        table,
                        columns * [1.5 * inch],
                        rows * [0.2 * inch]
                    )

                    doc = SimpleDocTemplate(temp_file, pagesize=letter)
                    doc.build([t])
                    temp_file.seek(0)

                    return Response(temp_file, mimetype='application/pdf')

            else:
                return 'Invalid Accept mimetype', 400
        else:
            print('non csv')
            return func(*args, **kwargs)

    return custom_func
