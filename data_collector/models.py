from sqlalchemy import *
from sqlalchemy.orm import (scoped_session, sessionmaker)

from sqlalchemy.ext.declarative import declarative_base

from data_collector.config import BaseConfig

engine = create_engine(BaseConfig.SQLALCHEMY_DATABASE_URI, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


class Uri(Base):
    __tablename__ = 'uri'

    short_uri = Column(String(20), primary_key=True)
    long_uri = Column(String(100), nullable=False)
    accessible = Column(Boolean)
    active = Column(Boolean, nullable=False)
    secure = Column(Boolean)
    admin = Column(Integer, nullable=False)

    def __repr__(self):
        return f'<URI {self.short_uri}>'


class UriAccess(Base):
    __tablename__ = 'uri_access'

    id = Column(Integer, primary_key=True)
    datetime = Column(DateTime, nullable=False)
    browser = Column(Text)
    os = Column(Text)
    ip = Column(Text)
    uri = Column(String(20), ForeignKey('uri.short_uri'), nullable=False)

    def __repr__(self):
        return f'<UriAccess {self.id}>'
