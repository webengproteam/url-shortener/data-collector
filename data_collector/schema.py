import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from data_collector.models import UriAccess as UriAccessModel
from data_collector.models import Uri as UriModel


class Uri(SQLAlchemyObjectType):
    class Meta:
        model = UriModel
        interfaces = (relay.Node, )


class UriAccess(SQLAlchemyObjectType):
    class Meta:
        model = UriAccessModel
        interfaces = (relay.Node, )


class UriAccessConnection(relay.Connection):
    class Meta:
        node = UriAccess


class Query(graphene.ObjectType):
    node = relay.Node.Field()
    # Allows sorting over multiple columns, by default over the primary key
    raw_access = graphene.List(UriAccess, user=graphene.Int())

    def resolve_raw_access(self, info, user=None):
        uri_query = Uri.get_query(info)
        accesses_query = UriAccess.get_query(info)

        if user:
            uris = uri_query.filter(UriModel.admin == user).all()
        else:
            uris = uri_query.all()

        accesses = []
        for uri in uris:
            accesses += accesses_query.filter(UriAccessModel.uri == uri.short_uri).all()

        return accesses


schema = graphene.Schema(query=Query)