FROM python:3-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN apk update && \
    apk add --no-cache zlib-dev jpeg-dev && \
    apk add --no-cache --virtual .build-deps build-base linux-headers && \
    pip3 install --no-cache-dir -r requirements.txt && \
    apk del .build-deps

COPY . /usr/src/app

EXPOSE 5000

ENTRYPOINT ["python3"]

CMD ["app.py"]
