# Data collector

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/data-collector/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/data-collector/commits/master)
**Test:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/data-collector/badges/test/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/data-collector/commits/test)

GraphQL server that provides aggregated and raw information about the clicks over time of URIs

## Local development

To set up the database run:

```bash
source set_db.sh
```

Then set the proper environmental variables with:

```bash
source set_env.sh local
```

At this point you can follow the steps indicated below for a Swagger server.

Later you can clean the database with:

```bash
source unset_db.sh
```

Take into consideration that you can't use this at the same time that docker-compose, since the
mapped ports are the same.

## Run server

To start the GraphQL server run:

```bash
pip3 install -r requirements.txt
python3 app.py
```

After this, the server will be available at `http://localhost:5000/graphql` and when browsing to it you'll see a web 
interface. There you can make queries such as:

```graphql
{
  all_users: rawAccess{
    uri
    datetime
    ip
    os
  }
  user_1: rawAccess(user:1){
    datetime
    ip
  }
}
```

Alternatively, it is possible to make requests in a URL-encoded format like:

```bash
curl -H "Accept: text/csv" "http://data.localhost/graphql?query=%7B%0A%20%20all_users%3A%20rawAccess%7B%0A%20%20%20%20uri%0A%20%20%20%20datetime%0A%20%20%20%20ip%0A%20%20%20%20os%0A%20%20%7D%0A%20%20user_1%3A%20rawAccess(user%3A1)%7B%0A%20%20%20%20datetime%0A%20%20%20%20ip%0A%20%20%7D%0A%7D%0A"
```

That will return something like this with `Accept: application/json`:

```json
{
  "data": {
    "all_users": [
      {
        "uri": "abc123",
        "datetime": "2019-11-05T09:15:23",
        "ip": "8.8.8.8",
        "os": "GNU/Linux"
      },
      {
        "uri": "abc123",
        "datetime": "2019-11-05T09:20:25",
        "ip": "8.8.4.4",
        "os": "GNU/Linux"
      },
      {
        "uri": "abc123",
        "datetime": "2019-11-05T09:22:33",
        "ip": "8.1.8.1",
        "os": "MacOS"
      },
      {
        "uri": "abc123",
        "datetime": "2019-11-05T09:30:45",
        "ip": "1.1.1.1",
        "os": "windows"
      },
      {
        "uri": "abc123",
        "datetime": "2019-11-05T10:15:23",
        "ip": "8.8.8.8",
        "os": "GNU/Linux"
      }
    ],
    "user_1": [
      {
        "datetime": "2019-11-05T09:15:23",
        "ip": "8.8.8.8"
      },
      {
        "datetime": "2019-11-05T09:20:25",
        "ip": "8.8.4.4"
      },
      {
        "datetime": "2019-11-05T09:22:33",
        "ip": "8.1.8.1"
      },
      {
        "datetime": "2019-11-05T09:30:45",
        "ip": "1.1.1.1"
      },
      {
        "datetime": "2019-11-05T10:15:23",
        "ip": "8.8.8.8"
      }
    ]
  }
}
```

And will return something like this with `Accept: text/csv`:

```csv
all_users
uri,datetime,ip,os
abc123,2019-11-05T09:15:23,8.8.8.8,GNU/Linux
abc123,2019-11-05T09:20:25,8.8.4.4,GNU/Linux
abc123,2019-11-05T09:22:33,8.1.8.1,MacOS
abc123,2019-11-05T09:30:45,1.1.1.1,windows
abc123,2019-11-05T10:15:23,8.8.8.8,GNU/Linux
user_1
datetime,ip
2019-11-05T09:15:23,8.8.8.8
2019-11-05T09:20:25,8.8.4.4
2019-11-05T09:22:33,8.1.8.1
2019-11-05T09:30:45,1.1.1.1
2019-11-05T10:15:23,8.8.8.8

```

If called with `Accept: application/pdf`, it will return the information of the CSV file
in a table in a PDF file.
